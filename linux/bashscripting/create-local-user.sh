#!/bin/bash
#
##########################################################################################
# FILE.........: create-local-user.sh 
# AUTHOR.......: Shehzad Shaikh (shehzadshaikh@live.in)
# VERSION......: v0.1
# CREATED......: 1st May 2022
# DESCRIPTION..: Script to create automated native linux/unix user with sudo permissions.
##########################################################################################
#
# set -x

# -------------------------- FUNCTION ---------------------
# NAME: print_usage
# DESCRIPTION: Print usage of the script
# ---------------------------------------------------------
print_usage(){
  echo -e "\nNAME"
  echo -e "\t${SCRIPT_CMD} - Create a native Linux user and set one time password.\n"
  echo "SYNOPSIS"
  echo -e "\tsudo ${SCRIPT_NAME} USERNAME [USERNAME...]\n"
  echo "DESCRIPTION"
  echo -e "\tCreate a native Linux user and provide sudoers capabilities.\n"
  echo "EXAMPLE"
  echo -e "\tsudo ${SCRIPT_NAME} dso-asylum"
  echo -e "\tsudo${SCRIPT_NAME} dso-dev dso-admin dso-sysops\n\n"

}

# -------------------------- FUNCTION ---------------------
# NAME: oneliner_usage
# DESCRIPTION: Print one liner script usage
# ---------------------------------------------------------
oneliner_usage(){
  echo -e "\nUSAGE: sudo ${SCRIPT_NAME} USERNAME [USERNAME...]\n"  
}

# -------------------------- FUNCTION --------------------------
# NAME: create_native_user
# DESCRIPTION: Create a native user and generate random password
# ----------------------------------------------------------------
create_native_user(){
    
  local username
  local password

  username=${1}
  password=$(openssl rand -base64 16)
    
  echo -e "\n\nCreating a user ${username}..."
  sleep 3
  adduser --gecos '' --quiet ${username} &> /dev/null << EOF
${password}
${password}
EOF
  
  if [[ ${?} -eq 0 ]]; then
    echo "Username: ${username}"
    echo "Password: ${password}"

    # Expire password upon first login
    passwd -e ${username} > /dev/null

    add_sudoers ${username}
  fi
}

# -------------------------- FUNCTION --------------------------
# NAME: add_sudoers
# DESCRIPTION: Function to provide sudo privileges to a user
# ----------------------------------------------------------------
add_sudoers(){
  
  local username
  username=${1}

  # Add user to sudoers file
  echo "${username} ALL=(ALL) NOPASSWD:ALL" > /tmp/${username}_sudoers
  visudo -cf /tmp/${username}_sudoers > /dev/null

  if [[ ${?} -eq 0 ]]; then
    mv /tmp/${username}_sudoers /etc/sudoers.d/
    chmod 400 /etc/sudoers.d/${username}_sudoers
  else
    echo "ERROR: Something went wrong while adding user to sudoers."
    exit 1
  fi
}

# -------------------------- FUNCTION ------------------------------------
# NAME: setup_user
# DESCRIPTION: Function to validate and add new user with sudo permissions
# -------------------------------------------------------------------------
setup_user(){
  
  local username
  username=${1}

  if grep -qE "^${username}" /etc/passwd &> /dev/null; then
    echo "INFO: User ${username} is already present on the system."

    if sudo -l -U ${username} &> /dev/null; then
      echo -e "INFO: User ${username} has sudoer permission.\n"
    else
      echo "INFO: Sudoer permission missing for user ${username}, adding sudoer privileges."
      add_sudoers ${username}
    fi
  else
    create_native_user ${username}
  fi
}

# ==============================================================
# ========================= MAIN ===============================
# ==============================================================
# Define variables here
SCRIPT_NAME=$(basename ${0})
SCRIPT_CMD=$(echo ${SCRIPT_NAME} | awk -F"." '{print $1}')

# Run script with sudo permissions
if [[ ${EUID} -ne 0 ]]; then
  echo -e "\nERROR: Please execute the script with sudo privileges."
  oneliner_usage
  exit 1
fi

# Check if enough argument supplied to the script.
if [[ ${#} -eq 0 ]]; then
  echo -e "\nERROR: No username supplied to the script."
  oneliner_usage
  exit 1
fi

for user in "${@}"
do
  case ${user} in
    # print help if user asks for it
    --help|-h|help|-help)
      print_usage
      exit 0
      ;;
    *)
      setup_user ${user}
      ;;
  esac
done

exit 0

